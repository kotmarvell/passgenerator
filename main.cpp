#include <iostream>
#include <string>
#include <cctype>
#include <fstream>
#include <ctime>

class PassGenerator
{
  bool useDigits;
  bool useLetter;
  bool useSpecSymbols;
  size_t minLength;
  size_t maxLength;

public:
  PassGenerator(bool, bool, bool, size_t, size_t);
  std::string Generate() const;
  void GenAndSave(const std::string &login, const std::string &path, const std::string &name) const;
};

PassGenerator::PassGenerator(bool b1, bool b2, bool b3, size_t t1, size_t t2)
{
  this->useDigits = b1;
  this->useLetter = b2;
  this->useSpecSymbols = b3;
  this->minLength = t1;
  this->maxLength = t2;
}

std::string PassGenerator::Generate() const
{
  srand((unsigned int)std::time(0));
  size_t length = rand() % this->maxLength + minLength;

  std::string result = "";

  while (result.length() < length)
  {
    char ch = rand() % 255;
    if (useDigits == true and isdigit(ch))
      result += ch;
    if (useLetter == true and std::isalpha(ch))
      result += ch;
    if (useSpecSymbols == true and ispunct(ch))
      result += ch;
  }
  return result;
}

void PassGenerator::GenAndSave(const std::string &login, const std::string &path, const std::string &name) const
{
  std::ofstream out(path, std::ios_base::app);
  if (!out.is_open())
  {
    throw "File not found";
  }
  out << name << " " << login << " " << this->Generate() << '\n';
  out.close();
}

int main()
{
  PassGenerator myPass(1, 1, 1, 40, 60);
  myPass.GenAndSave("Millie", "file.txt", "somelogin");
}
